from ctypes import *
import binascii
import os,sys
import csv

csvPath = "/Users/tzengyuxio/Pictures/SCAN/CSV/"
sourceRoot = "/Users/tzengyuxio/Pictures/SCAN/SOURCE/"
targetRoot = "/Users/tzengyuxio/Pictures/SCAN/TARGET/"
dupDir = "/Users/tzengyuxio/Pictures/SCAN/DUPLICATED/"

def computeFileCRC(filename):
	try:
		blocksize = 1024 * 64
		f = open(filename, "rb")
		str = f.read(blocksize)
		crc = 0
		while len(str) != 0:
			crc = binascii.crc32(str,crc) & 0xffffffff
			str = f.read(blocksize)
		f.close()
	except:
		print "compute file crc failed!"
		return 0
	return crc


# establish crcTable
crcTable = {}
crcCollisionCount = 0
for root, dirs, files in os.walk(csvPath):
	for f in files:
		if f[0] == '.':
			continue
		projectName = f[f.rfind('/')+1:f.rfind('.')]
		csvReader = csv.reader(open(root+f))
		print f
#		for fname, flen, fcrc, dir in csvReader:
		for row in csvReader:
			fname = row[0].replace('\\', '/').lower()
			flen = row[1]
			fcrc = '{:0>8}'.format(row[2].lower().replace("0x", ''))
			# ignore files without crc value
			if fcrc == "00000000":
#				print
#				print "crc error!!"
#				print "crc: %s" % fcrc
#				print "row: " + str(row)
				continue
			if row[3][:2] == "0x" or row[3][:2] == "0X":
				dir = ""
			else:
				dir = row[3].replace('\\', '/')
			if fcrc in crcTable:
				print
				print "crc collision!!"
				print "crc: %s [%s]" % (fcrc, fname)
				print "old: " + str(crcTable[fcrc])
				print "new: " + str((fname, flen, dir, projectName))
				crcCollisionCount += 1
				continue
#				exit()
			else:
				crcTable[fcrc] = (fname, flen, dir, projectName)


# establish csv files
csvTable = []
for root, dirs, files in os.walk(csvPath):
	for f in files:
		csvTable.append(f)


# iterative process of files in source dir
fCount = 0
cmdCount = 0
dupCount = 0
prevPass = True
for root, dirs, files in os.walk(sourceRoot):
	shortRoot = root.replace(sourceRoot, "")
	for f in files:
		# ignore hidden files
		if f[0] == '.':
			continue
		fCount += 1
		sourceFile = root+"/"+f
		flen = os.path.getsize(sourceFile)
#		fcrc = hex(computeFileCRC(sourceFile))[2:]
		fcrc = "%08x" % computeFileCRC(sourceFile)
		if fcrc in crcTable and str(flen) == crcTable[fcrc][1]:
			if prevPass:
				print ''
				prevPass = False
			projectDir = "%s%s" % (targetRoot, crcTable[fcrc][3])
			targetFile = "%s/%s/%s" % (projectDir, crcTable[fcrc][2], crcTable[fcrc][0])
			targetDir = targetFile[:targetFile.rfind('/')]
			# check if targetDir already exist
			if not os.path.isdir(targetDir):
				print "make dir: %s" % targetDir
				os.makedirs(targetDir)
			# check if targetFile already exist
			if os.path.isfile(targetFile):
				print "file already exist: %s" % targetFile.replace(targetRoot, "")
				shellCmd = 'mv "%s" "%s"' % (sourceFile, dupDir)
				retcode = os.system(shellCmd)
				dupCount += 1
				continue
			# copy file
			print "copy file: [%s] to [%s]" % (sourceFile.replace(sourceRoot, ""), crcTable[fcrc][0])
			shellCmd = 'mv "%s" "%s"' % (sourceFile, targetFile)
			retcode = os.system(shellCmd)
#			del crcTable[fcrc]
			cmdCount += 1
		else:
#			print '.',
			prevPass = True

print
print "%d files in CRC Table" % len(crcTable)
print "%d files crc collision!!" % crcCollisionCount
print "%d file(s) checked," % fCount
print "%d file(s) duplicated," % dupCount
print "%d file(s) copied." % cmdCount

#for crcRow in crcTable:
#    print str(crcTable[crcRow])